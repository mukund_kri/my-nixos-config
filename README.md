
# Backup of NixOS config 

## Status

I'v now reached a semi stable state on my nixos setup. Both my work laptop and 
home desktop run nixos now. This repo contains the config for both.

## Aim

To create a set of NixOS configs so that the software available across my 
computers is as uniform as possible.


## Usage

When I want to change/restore nixos all I do is soft-link the appropriate file 
from the machines folder to /etc/nixos/configuration.nix, and run nixos-* 
commands.

I do a similar kind of thing when I'm setting up a new computer.

## 9th Jun 2019 Update

Upgraded my Dell-Laptop to NixOS, 19.03. 
No problems with upgrade. Will roll it out to all computers.

