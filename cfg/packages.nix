{pkgs, ...}:
{
  environment.systemPackages = with pkgs; [

    # Utilites
    wget
    curl
    vim
    terminator
    tree
    unzip
    pavucontrol
    ntfs3g
    acpi

    # Emacs
    emacs
    source-code-pro
    aspell
    aspellDicts.en

    # Developmnet
    git
    jdk8
    python37Full
    vscode

    # XMonad
    dmenu
    xmobar

    # Applications
    firefox
    chromium
    clementine
    
  ];
}


