{ config, ... }:

{
  nixpkgs.config.allowUnfree = true; 

  # Set your time zone.
  time.timeZone = "Asia/Kolkata";

  # Firefox audio will not work without pulseaudio
  hardware.pulseaudio.enable = true;
}
