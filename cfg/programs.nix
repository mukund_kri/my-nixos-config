{ config, ... }:

{
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.bash.enableCompletion = true;
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };


  programs.zsh.enable = true;

  programs.zsh.ohMyZsh = {
    enable = true;
    plugins = ["git" "sudo" "docker" "kubctl"];
    theme = "agnoster";
  };

}
