{ config, pkgs, ... }:

{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.mukund = {
    isNormalUser = true;
    home = "/home/mukund";
    description = "Mukund K";
    extraGroups = [ "wheel" "networkmanager" "docker"];
  };


  users.users.mukund.shell = pkgs.zsh;
}
