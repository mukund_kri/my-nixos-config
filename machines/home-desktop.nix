# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      /etc/nixos/hardware-configuration.nix

      ../cfg/base.nix
      ../cfg/programs.nix
      ../cfg/services.nix
      ../cfg/users.nix

      # Packages to install by default
      ../cfg/packages.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sdb"; # or "nodev" for efi only

  networking.hostName = "homedesktop"; # Define your hostname.
  networking.networkmanager.enable = true;

  # Mount /dev/sda7 to /data. That's where I keep all my important stuff
  fileSystems."/data" = {
    device = "/dev/sda7";
    fsType = "ext4";
  };

  hardware.pulseaudio.enable = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "17.09"; # Did you read the comment?

}
